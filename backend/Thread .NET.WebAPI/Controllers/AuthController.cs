﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Services;
using Thread_.NET.Common.DTO.User;
using Thread_.NET.Validators;

namespace Thread_.NET.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [AllowAnonymous]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly AuthService _authService;

        public AuthController(AuthService authService)
        {
            _authService = authService;
        }

        [HttpPost("login")]
        public async Task<ActionResult<AuthUserDTO>> Login(UserLoginDTO dto)
        {
            UserLoginDTOValidator userValidator = new UserLoginDTOValidator();
            var validationResult = userValidator.Validate(dto);

            if (!validationResult.IsValid)
            {
                throw new InvalidDataExeption<UserLoginDTO>();
            }

            return Ok(await _authService.Authorize(dto));
        }

        private Exception InvalidUsernameOrPasswordException()
        {
            throw new NotImplementedException();
        }
    }
}