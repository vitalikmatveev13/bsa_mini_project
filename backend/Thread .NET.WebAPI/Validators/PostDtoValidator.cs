﻿using FluentValidation;
using System.Text.RegularExpressions;
using Thread_.NET.Common.DTO.Post;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class PostDTOValidator : AbstractValidator<PostCreateDTO>
    {
        public PostDTOValidator()
        {
            RuleFor(u => u.Body).NotNull();
        }
    }
}
