﻿using FluentValidation;
using System.Text.RegularExpressions;
using Thread_.NET.Common.DTO.User;

namespace Thread_.NET.Validators
{
    public sealed class UserLoginDTOValidator : AbstractValidator<UserLoginDTO>
    {
        public UserLoginDTOValidator()
        {
            RuleFor(u => u.Email).NotNull();
            RuleFor(u => u.Email).Must(e=>Regex.IsMatch(e, emailPatter, RegexOptions.IgnoreCase));
            RuleFor(u => u.Password).NotNull();
        }

        string emailPatter = @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$";
    }
}
