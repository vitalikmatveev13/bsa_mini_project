﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thread_.NET.BLL.Exceptions
{
    public class InvalidDataExeption<T> : Exception
    {
        public InvalidDataExeption() : base($"Ivalid data for {typeof(T).Name}")
        {

        }
    }
}
