﻿using AutoMapper;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Thread_.NET.BLL.Exceptions;
using Thread_.NET.BLL.Hubs;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Comment;
using Thread_.NET.DAL.Context;
using Thread_.NET.DAL.Entities;

namespace Thread_.NET.BLL.Services
{
    public sealed class CommentService : BaseService
    {
        private readonly IHubContext<CommentHub> _commentHub;
        public CommentService(ThreadContext context, IMapper mapper, IHubContext<CommentHub> commentHub) : base(context, mapper) 
        {
            _commentHub = commentHub;
        }

        public async Task<CommentDTO> CreateComment(NewCommentDTO newComment)
        {
            var commentEntity = _mapper.Map<Comment>(newComment);

            _context.Comments.Add(commentEntity);
            await _context.SaveChangesAsync();

            var createdComment = await _context.Comments
                .Include(comment => comment.Author)
                    .ThenInclude(user => user.Avatar)
                .FirstAsync(comment => comment.Id == commentEntity.Id);

            _commentHub.Clients.All.SendAsync("NewComment", newComment);

            return _mapper.Map<CommentDTO>(createdComment);
        }

        public async Task UpdateComment(CommentDTO comment)
        {
            var currentComment = await _context.Comments.FirstOrDefaultAsync(c => c.Id == comment.Id);

            if (currentComment != null)
            {
                currentComment.Body = comment.Body;
                currentComment.UpdatedAt = System.DateTime.Now;
                _context.Update(currentComment);
                await _context.SaveChangesAsync();
            }
        }

        public async Task DeleteComment(int id)
        {
            if(id == 0)
            {
                throw new NotFoundException(nameof(Comment), id);
            }

            var comment = await _context.Comments.FirstOrDefaultAsync(c=>c.Id == id);
            if (comment != null)
            {
                _context.Comments.Remove(comment);
                await _context.SaveChangesAsync();
            }
            
        }
    }
}
