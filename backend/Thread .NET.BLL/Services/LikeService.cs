﻿using AutoMapper;
using System.Linq;
using System.Threading.Tasks;
using Thread_.NET.BLL.Services.Abstract;
using Thread_.NET.Common.DTO.Like;
using Thread_.NET.DAL.Context;

namespace Thread_.NET.BLL.Services
{
    public sealed class LikeService : BaseService
    {
        public LikeService(ThreadContext context, IMapper mapper) : base(context, mapper) { }

        public async Task LikePost(NewReactionDTO reaction)
        {
            var likes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (likes.Any())
            {
                var changeReaction = likes.All(likes => likes.IsLike == false);
                _context.PostReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                //if we change reaction from dislike to like
                if (changeReaction)
                {
                    _context.PostReactions.Add(CreatePostReaction(reaction));
                    await _context.SaveChangesAsync();
                }

                return;
            }

            _context.PostReactions.Add(CreatePostReaction(reaction));

            await _context.SaveChangesAsync();
        }
        public async Task DisLikePost(NewReactionDTO reaction)
        {
            var disLikes = _context.PostReactions.Where(x => x.UserId == reaction.UserId && x.PostId == reaction.EntityId);

            if (disLikes.Any())
            {
                var changeReaction = disLikes.All(dl => dl.IsLike == true);
                _context.PostReactions.RemoveRange(disLikes);
                await _context.SaveChangesAsync();

                //if we change reaction from like to dislike
                if(changeReaction)
                {
                    _context.PostReactions.Add(CreatePostReaction(reaction));
                    await _context.SaveChangesAsync();
                }
            }

            _context.PostReactions.Add(CreatePostReaction(reaction));

            await _context.SaveChangesAsync();
        }

        public async Task LikeComment(NewReactionDTO reaction)
        {
            var likes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (likes.Any())
            {
                var changeReaction = likes.All(likes => likes.IsLike == false);
                _context.CommentReactions.RemoveRange(likes);
                await _context.SaveChangesAsync();

                //if we change reaction from dislike to like
                if (changeReaction)
                {
                    _context.CommentReactions.Add(CreateCommentReaction(reaction));
                    await _context.SaveChangesAsync();
                }

                return;
            }

            _context.CommentReactions.Add(CreateCommentReaction(reaction));

            await _context.SaveChangesAsync();
        }
        public async Task DisLikeComment(NewReactionDTO reaction)
        {
            var disLikes = _context.CommentReactions.Where(x => x.UserId == reaction.UserId && x.CommentId == reaction.EntityId);

            if (disLikes.Any())
            {
                var changeReaction = disLikes.All(dl => dl.IsLike == true);
                _context.CommentReactions.RemoveRange(disLikes);
                await _context.SaveChangesAsync();

                //if we change reaction from like to dislike
                if (changeReaction)
                {
                    _context.CommentReactions.Add(CreateCommentReaction(reaction));
                    await _context.SaveChangesAsync();
                }
            }

            _context.CommentReactions.Add(CreateCommentReaction(reaction));

            await _context.SaveChangesAsync();
        }

        private DAL.Entities.PostReaction CreatePostReaction(NewReactionDTO reaction)
        {
            return new DAL.Entities.PostReaction
            {
                PostId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId,
            };
        }

        private DAL.Entities.CommentReaction CreateCommentReaction(NewReactionDTO reaction)
        {
            return new DAL.Entities.CommentReaction
            {
                CommentId = reaction.EntityId,
                IsLike = reaction.IsLike,
                UserId = reaction.UserId,
            };
        }
    }
}
