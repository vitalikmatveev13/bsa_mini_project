import { Injectable } from '@angular/core';
import { HttpInternalService } from './http-internal.service';
import { NewComment } from '../models/comment/new-comment';
import { Comment } from '../models/comment/comment';
import { CommaExpr } from '@angular/compiler';
import { NewReaction } from '../models/reactions/newReaction';


@Injectable({ providedIn: 'root' })
export class CommentService {
    public routePrefix = '/api/comments';

    constructor(private httpService: HttpInternalService) { }

    public createComment(post: NewComment) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}`, post);
    }

    public editComment(comment: Comment) {
        return this.httpService.putRequest(`${this.routePrefix}`, comment);
    }

    public deleteComment(id) {
        return this.httpService.deleteRequest(`${this.routePrefix}/${id}`);
    }

    public likeComment(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/like`, reaction);
    }
    public disLikePost(reaction: NewReaction) {
        return this.httpService.postFullRequest<Comment>(`${this.routePrefix}/dislike`, reaction);
    }
}
