import { Component, Inject, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogOverviewDialog } from '../dialog-overview-dialog/dialog-overview-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { User } from 'src/app/models/user';
import { Reaction } from 'src/app/models/reactions/reaction';

export interface DialogData {
    // users: User[]
    userReactions: Reaction[]
}

/**
 * @title Dialog Overview
 */
@Component({
    selector: 'dialog-overview',
    templateUrl: './dialog-overview.component.html',
})
export class DialogOverview {

    @Input() reactions: Reaction[]
    constructor(public dialog: MatDialog) { }

    openDialog(): void {
        let users: User[] = [];
        // for (let index = 0; index < this.reactions.length; index++) {
        //     let x = this.reactions[index].user;
        //     users.push(this.reactions[index].user as User);
        // }
        const dialogRef = this.dialog.open(DialogOverviewDialog, {
            width: '250px',
            data: { userReactions: this.reactions.sort((a, b) => a.isLike ? 1 : -1) },
        });

        dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed');
        });
    }
}