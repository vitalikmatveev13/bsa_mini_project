import { Component, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { Post } from 'src/app/models/post/post';
import { User } from 'src/app/models/user';
import { CommentService } from 'src/app/services/comment.service';
import { LikeService } from 'src/app/services/like.service';
import { UserService } from 'src/app/services/user.service';
import { Comment } from '../../models/comment/comment';
import { AuthenticationService } from '../../services/auth.service';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public post: Post;
    @Input() public comment: Comment;
    @Input() public currentUser: User;

    private unsubscribe$ = new Subject<void>();

    public isEditable = false;

    public constructor(
        private authService: AuthenticationService,
        private userService: UserService,
        private commentService: CommentService,
        private likeService: LikeService,) {

    }
    public isOwner(): boolean {
        if (this.currentUser === undefined) {
            return false;
        }
        return this.currentUser.id === this.comment.author.id
    }

    public editComment() {
        this.isEditable = true;
    }

    public sendComment() {
        this.commentService.editComment(this.comment).subscribe();
        this.isEditable = false;
    }

    public deleteComment() {
        if (confirm("Are you sure?")) {
            this.commentService.deleteComment(this.comment.id).subscribe();
            this.post.comments = this.post.comments.filter(c => c.id !== this.comment.id);
        }

    }

    public likeComment() {
        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public disLikeComment() {
        this.likeService
            .disLikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => (this.comment = comment));
    }

    public reactionCount(isLike: boolean): number {
        var reactions = this.comment.reactions.filter(r => r.isLike == isLike)
        return reactions.length;
    }
}
