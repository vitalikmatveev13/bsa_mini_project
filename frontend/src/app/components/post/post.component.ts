import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Post } from '../../models/post/post';
import { AuthenticationService } from '../../services/auth.service';
import { AuthDialogService } from '../../services/auth-dialog.service';
import { empty, Observable, Subject } from 'rxjs';
import { DialogType } from '../../models/common/auth-dialog-type';
import { LikeService } from '../../services/like.service';
import { NewComment } from '../../models/comment/new-comment';
import { CommentService } from '../../services/comment.service';
import { User } from '../../models/user';
import { Comment } from '../../models/comment/comment';
import { catchError, delay, switchMap, takeUntil } from 'rxjs/operators';
import { SnackBarService } from '../../services/snack-bar.service';
import { PostService } from 'src/app/services/post.service';
import { identifierModuleUrl } from '@angular/compiler';
import { UserService } from 'src/app/services/user.service';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { environment } from 'src/environments/environment';
import { ToastrService } from 'ngx-toastr';

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.sass']
})
export class PostComponent implements OnDestroy, OnInit {
    @Input() public post: Post;
    @Input() public currentUser: User;

    @Output() onChange = new EventEmitter<Post[]>();

    public showComments = false;
    public editeblePost = false;
    public accessForChanging = false;
    public newComment = {} as NewComment;
    public commentHub: HubConnection;
    public hasNewComments = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private postService: PostService,
        private toastrService: ToastrService
    ) {
    }
    ngOnInit(): void {
        this.registerHub();
    }

    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    public toggleComments() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((user) => {
                    if (user) {
                        this.currentUser = user;
                        this.showComments = !this.showComments;
                        this.readCommets()
                    }
                });
            return;
        }

        this.showComments = !this.showComments;
        this.readCommets();
    }

    public likePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .likePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));
        this.toastrService.success('Post was liked');
    }

    public disLikePost() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.disLikePost(this.post, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => (this.post = post));

            return;
        }

        this.likeService
            .disLikePost(this.post, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => (this.post = post));

        this.toastrService.error('Post was disliked');
    }

    public sendComment() {
        this.newComment.authorId = this.currentUser.id;
        this.newComment.postId = this.post.id;

        this.commentService
            .createComment(this.newComment)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe(
                (resp) => {
                    if (resp) {
                        this.post.comments = this.sortCommentArray(this.post.comments.concat(resp.body));
                        this.newComment.body = undefined;
                    }
                },
                (error) => this.snackBarService.showErrorMessage(error)
            );
        this.toastrService.success("Comment added");
    }

    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public IsEditPost() {
        this.editeblePost = !this.editeblePost;
    }

    public editPost() {
        this.postService.editPost(this.post).subscribe();
        this.IsEditPost();
    }

    public tryDelete() {
        if (this.currentUser.id === this.post.author.id) {
            if (confirm("Are you sure?")) {
                this.postService.deletePost(this.post.id).subscribe();
                //window.location.reload();
            }
        }
    }

    public reactionCount(isLike: boolean): number {
        var reactions = this.post.reactions.filter(r => r.isLike == isLike)
        return reactions.length;
    }

    public isOwner(): boolean {
        if (this.currentUser === undefined) {
            return false;
        }
        return this.currentUser.id === this.post.author.id
    }
    public registerHub() {
        this.commentHub = new HubConnectionBuilder().withUrl(`${environment.apiUrl}/notifications/comment`).build();
        this.commentHub.start().catch((error) => this.snackBarService.showErrorMessage(error));

        this.commentHub.on('NewComment', (newComment: NewComment) => {
            if (newComment && newComment.postId === this.post.id) {
                this.newCommentNotification();
            }
        });
    }
    public newCommentNotification() {
        this.hasNewComments = true;
    }

    private readCommets() {
        this.hasNewComments = false;
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }

    private sortCommentArray(array: Comment[]): Comment[] {
        return array.sort((a, b) => +new Date(b.createdAt) - +new Date(a.createdAt));
    }
}
